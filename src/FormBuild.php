<?php

namespace RenatoMelo\Form;

use RenatoMelo\Form\Traits\FormCallsTrait;
use Illuminate\Support\Str;

class FormBuild
{
    use FormCallsTrait;

    const CLASS_INPUT_ERROR = 'input_error';
    const CLASS_TEXT_ERROR = 'text-danger';


    protected $name;
    protected $label;
    protected $id;
    protected $attributes;
    protected $options;
    protected $checked;
    protected $value;
    protected $classTextError = self::CLASS_TEXT_ERROR;
    protected $classLabel;
    protected $classError = self::CLASS_INPUT_ERROR;
    protected $comErro;
    protected $quantErros = 1;
    protected $tipo;
    protected $model;

    protected $icone;

    public function __construct($name, $tipo, $model = null)
    {
        $this->model = $model;
        $this->tipo = $tipo;
        $this->name = str_replace(['[', ']'], ['parentesesopen', 'parentesesclose'], $name);
        $this->name = Str::slug($this->name, '_');
        $this->id = Str::slug($name, '_');
        $this->name = str_replace(['parentesesopen', 'parentesesclose'], ['[', ']'], $this->name);
        $this->label = $this->label ?? str_replace(['_', '-'], ' ', Str::ucfirst($name, '_'));
    }

    protected function getErrors()
    {

        $nome = str_replace('[]', '', $this->name);
        if ($this->tipo != 'label') {
            if (session('errors')) {
                $errors = session('errors')->get($nome) ?? false;

                if ($errors) {
                    $this->comErro = true;
                    $htmlErro = '';
                    foreach ($errors as $key => $error) {
                        if ($key + 1 <= $this->quantErros) {
                            $htmlErro .= rtrim($error, '.') . "! ";
                        }
                    }
                    return '<div class="' . $this->classTextError . '">' . $htmlErro . '</div>';
                }
            }
        }
    }

    protected function attributesToHtml()
    {
        $html = '';
        if ($this->comErro) {
            $this->attributes['class'] .= ' ' . $this->classError;
        }
        if ($this->tipo != 'hidden') {
            if (isset($this->model['disabled'])) {
                $this->attributes['disabled'] = 'disabled';
            }
            if (isset($this->model['readonly'])) {
                $this->attributes['readonly'] = 'readonly';
            }
        }

        foreach (($this->attributes ?? []) as $key => $value) {
            $html .= " {$key}='{$value}'";
        }

        return $html;
    }

    protected function build()
    {
        //processa os erros para usar nos HTML dos inputs
        $erros = $this->getErrors();
        switch ($this->tipo) {
            case 'text':
                $htmlInput = '<input ' . ($this->label && !isset($this->attributes['id']) ? 'id="' . $this->id . '"' : '') . ' type="text" name="' . $this->name . '" value="' . $this->getValue() . '" ' . $this->attributesToHtml() . ' />';
                break;
            case 'password':
                $htmlInput = '<input ' . ($this->label && !isset($this->attributes['id']) ? 'id="' . $this->id . '"' : '') . ' type="password" name="' . $this->name . '" value="' . $this->getValue() . '" ' . $this->attributesToHtml() . ' />';
                break;
            case 'hidden':
                $htmlInput = '<input type="hidden" name="' . $this->name . '" value="' . $this->getValue() . '" ' . $this->attributesToHtml() . ' />';
                $this->label = false;
                break;
            case 'textarea':
                $htmlInput = '<textarea ' . ($this->label && !isset($this->attributes['id']) ? 'id="' . $this->id . '"' : '') . ' name="' . $this->name . '" ' . $this->attributesToHtml() . '>' . $this->getValue() . '</textarea>';
                break;
            case 'select':
                $htmlInput = '<select ' . ($this->label && !isset($this->attributes['id']) ? 'id="' . $this->id . '"' : '') . ' name="' . $this->name . '" ' . $this->attributesToHtml() . '>' . $this->getOptions() . '</select>';
                break;
            case 'radio':
            case 'checkbox':
                $htmlInput = $this->htmlChecklabe();
                break;
            case 'submit':
                $htmlInput = '<button type="submit" ' . $this->attributesToHtml() . '>' . ($this->icone ?? '') . $this->label . '</button>';
                $this->label = false;
                break;
        }
        return $this->getLabel() . ($htmlInput ?? null) . $erros;
    }

    public function getLabel()
    {
        return $this->label !== false ? '<label for="' . ($this->attributes['id'] ?? $this->id) . '" ' . ($this->classLabel ? ' class="' . $this->classLabel . '"' : '') . '>' . $this->label . '</label><br>' : '';
    }

    protected function getValue()
    {

        //dd(request()->all(),$this->name, old($this->name));
        if (old($this->name)) {
            $value = old($this->name);
        } elseif (isset($this->model['model']->{$this->name})) {
            $value = $this->model['model']->{$this->name};
        } else {
            $value = $this->value;
        }

        if (count(old()) != 0 && !old($this->name)) {
            $value = null;
        }

        return $value ?? null;
    }

    public function __toString()
    {
        return $this->build();
    }


}
