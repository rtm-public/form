<?php

namespace RenatoMelo\Form\Traits;

trait TextAreaTrait
{
    public function cols(int $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

    public function rows(int $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

    public function maxlength(int $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

    public function minlength(int $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

    public function wrap(string $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

}
