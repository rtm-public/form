<?php

namespace RenatoMelo\Form\Traits;

trait SelectTrait
{
    public function getOptions()
    {
        $options = $this->options;
        $html = '<option></option>';
        if (isset($this->attributes['placeholder'])) {
            $html .= '<option hidden>' . $this->attributes['placeholder'] . '...</option>';
        }

//        if ($this->firstOptionsEmpty) {
//            $html .= '<option></option>';
//        }

        foreach (($options ?? []) as $key => $value) {
            $html .= '<option ' . ($this->isSelected($key) == $key ? 'selected' : '') . ' value="' . $key . '">' . $value . '</option>';
        }
        return $html;
    }

    private function isSelected($value)
    {
        $nome = str_replace(['[', ']'], [''], $this->name);
        //dd($value);
        if (count(old()) != 0 && !old($nome)) {
            return false;
        } elseif (old($nome) && old($nome) == $value) {
            return true;
        } elseif (old($nome) && is_array(old($nome)) && in_array($value, old($nome))) {
            return true;
        } elseif (!old($nome) && isset($this->model['model']->{$nome}) && $this->model['model']->{$nome} == $value) {
            return true;
        } elseif (!old($nome) && isset($this->model['model']->{$nome}) && is_array($this->model['model']->{$nome}) && in_array($value, $this->model['model']->{$nome})) {
            return true;
        } elseif (count(old()) == 0 && !isset($this->model) && $this->value == $value) {
            return true;
        }elseif (count(old()) == 0 && $this->value == $value) {
            return true;
        }
        return false;
    }
}
