<?php

namespace RenatoMelo\Form\Traits;

use Illuminate\Support\Str;

trait FormCallsTrait
{
    use TextAreaTrait;
    use SelectTrait;
    use ChecklabeTrait;
    use IconeTrait;

    public function when(bool $condicao, callable $callback){
        if ($condicao) {
            $callback($this);
        }
        return $this;
    }
    public function label(bool|string $label)
    {
        if ($label === false || $this->tipo == 'HIDDEN') {
            $this->label = false;
        } else {
            $this->label = Str::ucfirst($label);
        }
        return $this;
    }

    public function class(string $class)
    {
        $this->attributes['class'] = trim(($this->attributes['class'] ?? '') . " " . $class);
        return $this;
    }

    public function id(string $id)
    {
        $this->attributes['id'] = $id;
        return $this;
    }

    public function style(string ...$styles)
    {
        foreach ($styles as $style) {
            $this->attributes['style'] = trim(ltrim(($this->attributes['style'] ?? '') . "; " . $style), ';');
        }
        return $this;
    }

    public function dataAttr(string ...$attrs)
    {
        foreach ($attrs as $attr) {
            $params = preg_split('/[:=]/', $attr);
            $this->attributes['data-' . str_replace('data-', '', $params[0])] = trim($params[1] ?? $params[0]);
        }
        return $this;
    }

    public function attr(string ...$attrs)
    {
        foreach ($attrs as $attr) {
            $params = preg_split('/[:=]/', $attr);
            $this->attributes[$params[0]] = trim($params[1] ?? $params[0]);
        }
        return $this;
    }

    public function live(string ...$attrs) // live('class.value')
    {
        foreach ($attrs as $attr) {
            $this->attributes['data-live'] = trim(trim(($this->attributes['data-live'] ?? '') . "|" . $attr),"|");
        }
       $this->class('live');

        return $this;
    }


    public function value(string $value)
    {
        $this->value = $value;
        return $this;
    }

    public function classError(string $class)
    {
        if ($this->classError == self::CLASS_INPUT_ERROR) {
            $this->classError = '';
        }
        $this->classError = trim(($this->classError ?? '') . " " . $class);
        return $this;
    }

    public function option()
    {
        $args = func_get_args();
        $options = [];
        foreach ($args as $arg) {
            if (is_array($arg)) {
                $options = $options + $arg;
            } elseif (is_string($arg)) {
                $options[$arg] = $arg;
            }
        }
        $this->options = $options;

        return $this;
    }
    public function checked(bool $value = true)
    {
        $this->checked = $value;
        return $this;
    }

    public function classLabel(string $class)
    {
        $this->classLabel = trim(($this->classLabel ?? '') . " " . $class);
        return $this;
    }

    public function classTextError(string $class)
    {
        if ($this->classTextError == self::CLASS_TEXT_ERROR) {
            $this->classTextError = '';
        }
        $this->classTextError = trim(($this->classTextError ?? '') . " " . $class);
        return $this;
    }

    public function erros(int|string $quant = null)
    {
        if ($quant === false) {
            $this->quantErros = 0;
        } else {
            $this->quantErros = $quant ?? $this->quantErros;
        }
        return $this;
    }

    public function form(string $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

    public function placeholder(string $value)
    {
        $this->attributes[__FUNCTION__] = $value;
        return $this;
    }

    public function autocomplete(bool $value = true)
    {
        if ($value) {
            $this->attributes[__FUNCTION__] = 'on';
        } else {
            $this->attributes[__FUNCTION__] = 'off';
        }
        return $this;
    }

    public function required(bool $value = true)
    {
        return self::attributesBooleans(__FUNCTION__, $value);
    }

    public function readonly(bool $value = true)
    {
        return self::attributesBooleans(__FUNCTION__, $value);
    }

    public function disabled(bool $value = true)
    {
        return self::attributesBooleans(__FUNCTION__, $value);
    }

    public function autofocus(bool $value = true)
    {
        return self::attributesBooleans(__FUNCTION__, $value);
    }

    protected function attributesBooleans($attr, $value)
    {
        if ($value) {
            $this->attributes[$attr] = $attr;
        } else {
            unset($this->attributes[$attr]);
        }
        return $this;
    }
}
