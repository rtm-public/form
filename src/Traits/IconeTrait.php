<?php

namespace RenatoMelo\Form\Traits;

trait IconeTrait
{
    public function icone(string $value)
    {
        $this->icone = ' <i class="'.$value.'"></i> ';
        return $this;
    }
}
