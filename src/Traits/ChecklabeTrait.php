<?php

namespace RenatoMelo\Form\Traits;

trait ChecklabeTrait
{
    public function htmlChecklabe()
    {
        $html = '';
//dd($this, request()->all());
        if ($this->options) {
            $options = $this->options;
            $html = '';

            foreach (($options ?? []) as $key => $value) {
                //dd($key);
                $html .= '<label><input name="' . $this->name . '" ' . ($this->isChecked($key) ? 'checked' : '') . '  value="' . $key . '" ' . $this->attributesToHtml() . ' type="' . $this->tipo . '" /> ' . $value . '</label><br>';
            }
        } else {

            $html .= '<label><input name="' . $this->name . '" ' . ($this->isChecked($this->value) ? 'checked' : '') . '  value="' . $this->value . '" ' . $this->attributesToHtml() . ' type="' . $this->tipo . '" /> ' . $this->label . '</label><br>';
            if ($this->label) {
                $this->label = false;
            }
        }

        return $html;
    }

    private function isChecked($value)
    {
        $nome = str_replace(['[', ']'], [''], $this->name);
        //dd($this->name, old(), old($nome)? true : false, $nome);
        if (count(old()) != 0 && !old($nome)) {
            return false;
        } elseif (old($nome) && old($nome) == $value) {
            return true;
        } elseif (old($nome) && is_array(old($nome)) && in_array($value, old($nome))) {
            return true;
        } elseif (!old($nome) && isset($this->model['model']->{$nome}) && $this->model['model']->{$nome} == $value) {
            return true;
        } elseif (!old($nome) && isset($this->model['model']->{$nome}) && is_array($this->model['model']->{$nome}) && in_array($value, $this->model['model']->{$nome})) {
            return true;
        } elseif (count(old()) == 0 && !isset($this->model) && $this->checked) {
            return true;
        }
        return false;
    }
}
