<?php

namespace RenatoMelo\Form;

class Form extends FormBase
{
    public function disabled()
    {
        static::$model['disabled'] = 'disabled';
        return $this;
    }
    public function readonly()
    {
        static::$model['readonly'] = 'readonly';
        return $this;
    }

    public static function text(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function hidden(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function password(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function select(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function textarea(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function checkbox(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function radio(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function label(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

    public static function submit(string $name)
    {
        return new FormBuild($name, __FUNCTION__, self::$model);
    }

}
