<?php

namespace RenatoMelo\Form;
abstract class FormBase {
    protected static $model = null;

    public static function model(object $model) {
        static::$model['model'] = $model;
        return new static();
    }
}
