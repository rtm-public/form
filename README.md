# RenatoMelo/Form

## Para instalar
```
"repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/rtm-public/form.git"
        }
    ]
```


` $_> composer require renatomelo/form `

### Erro na instalação

execute antes esse comando

` $_> composer config -g repo.packagist composer https://packagist.org `


## Como usar


# Form
Pacote PHP para geração de HTML para inputs de formulários

### Tipos de inputs disponiveis:

```
Form::text();
Form::password();
Form::hidden();
Form::textarea();
Form::select();
Form::radio();
Form::checkbox();
Form::label();
Form::model();
```
<hr>

### Utilização:
A Classe possui os médotos estáticos, não precisa instânciar um novo objeto para utilizar.

**Ex.:**<br>
` $input = Form::text('campo');`<br>

A tag `<.../>` nessa documentação indica o resultado em HTML gerado pelo código PHP,

`<.../>`<br>
`  <input type="text" name="campo" /> `
<hr>

### Padrões de retorno


- Os tipos de inputs text(), password(), select() e textarea() gerará o atributo **name** que será o parâmetro do input em snake case.
- Os tipo de inputs **radio()** e **checkbox()** tem comportamentos diferentes do label que serão tratados nas seções desses inputs.
`  Form::text('Titulo do Campo'); ` 

`<.../>`<br>
`  <input type="text" name="titulo_do_campo" /> `

<br>

- Todos os tipos de inputs gerará uma tag `<label>` com o atributo **for** antes do input. O conteúdo do `<label>` será o valor do parâmetro do input formatado para retirar underscore(_) e hifen (-) e captalizar a primeira letra da string.
O valor do atributo **for** será o mesmo valor do atributo **name** do input. Também gerará um atributo **id** para o input com o mesmo valor do atributo **name**

` Form::text('Titulo do Campo'); ` 

`<.../>`<br>
` <label for="titulo_do_campo">Titulo do Campo</label>`<br>
`  <input id="titulo_do_campo" type="text" name="titulo_do_campo" /> `

<br>

- Caso queira trocar o texto do label chame o metodo `->label()`.

` Form::text('Titulo do Campo')->label('Meu Label'); ` 

`<.../>`<br>
` <label for="titulo_do_campo">Meu Label</label>`<br>
`  <input id="titulo_do_campo" type="text" name="titulo_do_campo" /> `


<br>

- Caso não deseje um label chame o método `->label(false)` e passe **false** no parâmetro.

` Form::text('Titulo do Campo')->label(false); ` 

`<.../>`<br>
`  <input type="text" name="titulo_do_campo" /> `
